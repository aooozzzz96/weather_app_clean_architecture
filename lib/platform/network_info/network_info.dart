abstract class NetworkInfo{
  Future get isConnected;
}