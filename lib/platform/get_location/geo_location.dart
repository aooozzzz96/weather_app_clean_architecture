import 'package:location/location.dart';

abstract class GeoLocation {
  Future<LocationData> get location;
  Future<bool> get serviceEnabled;
  Future<bool> get requestService;
  Future<PermissionStatus> get hasPermission;
  Future<PermissionStatus> get requestPermission;
}