import 'package:dartz/dartz.dart';
import 'package:myapp/features/domain/entities/Failure.dart';
import 'package:myapp/features/domain/entities/Weather.dart';
import 'package:myapp/features/domain/repositories/weather_repository.dart';

class GetWeatherThisDay {
  final WeatherRepository weatherRepository;
  GetWeatherThisDay({this.weatherRepository});

  Future<Either<Failure, Weather>> call() {
    return weatherRepository.getWeatherThisDay();
  }
}