import 'package:dartz/dartz.dart';
import 'package:myapp/features/domain/entities/Failure.dart';
import 'package:myapp/features/domain/entities/Weather.dart';
import 'package:myapp/features/domain/repositories/weather_repository.dart';

class GetWeatherForecast {
  final WeatherRepository weatherRepository;
  const GetWeatherForecast({this.weatherRepository});

  Future<Either<Failure, List<Weather>>> call() {
    return weatherRepository.getWeatherForecast();
  }
}