import 'package:dartz/dartz.dart';
import 'package:myapp/features/domain/entities/Failure.dart';
import 'package:myapp/features/domain/entities/Weather.dart';

abstract class WeatherRepository{
  Future<Either<Failure, Weather>> getWeatherThisDay();
  Future<Either<Failure, List<Weather>>> getWeatherForecast();
}