import 'package:equatable/equatable.dart';
import 'package:weather/weather.dart' as we;

class Weather extends Equatable{
  final we.Weather weather;
  const Weather({this.weather});
  @override
  // TODO: implement props
  List<Object> get props => [weather];
}