import 'package:equatable/equatable.dart';

class Failure extends Equatable{
  final String name;
  const Failure({this.name: ""});

  @override
  // TODO: implement props
  List<Object> get props => [name];
}

class ServerFailure extends Failure{
  ServerFailure({String name: ""}) : super(name: name);
}

class LocationFailure extends Failure{
  LocationFailure({String name: ""}) : super(name: name);
}

class CacheFailure extends Failure{
  CacheFailure({String name: ""}) : super(name: name);
}