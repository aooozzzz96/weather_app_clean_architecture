import 'package:dartz/dartz.dart';
import 'package:location/location.dart';
import 'package:myapp/features/data/data_sources/weather_data_remote_data_source.dart';
import 'package:myapp/features/data/data_sources/weather_data_local_data_source.dart';
import 'package:myapp/features/data/models/exceptions.dart';
import 'package:myapp/features/domain/entities/Failure.dart';
import 'package:myapp/features/domain/entities/Weather.dart';
import 'package:myapp/features/domain/repositories/weather_repository.dart';
import 'package:myapp/platform/get_location/geo_location.dart';
import 'package:myapp/platform/network_info/network_info.dart';
import 'package:weather/weather.dart' as we;

class WeatherRepositoryImpl extends WeatherRepository {
  final GetWeatherRemoteDataSource getWeatherRemoteDataSource;
  final GetWeatherLocalDataSource getWeatherLocalDataSource;
  final NetworkInfo networkInfo;
  final GeoLocation geoLocation;

  WeatherRepositoryImpl(
      {this.getWeatherRemoteDataSource,
      this.getWeatherLocalDataSource,
      this.geoLocation,
      this.networkInfo});

  @override
  Future<Either<Failure, List<Weather>>> getWeatherForecast() {
    // TODO: implement getWeatherForecast
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, Weather>> getWeatherThisDay() async {
    // TODO: implement getWeatherThisDay
    if (await networkInfo.isConnected) {
      PermissionStatus permissionStatus = await geoLocation.hasPermission;
      if (permissionStatus == PermissionStatus.granted || permissionStatus == PermissionStatus.grantedLimited) {
        if (await geoLocation.serviceEnabled) {
          LocationData locationData = await geoLocation.location;
          return _getWeatherOfDayRemote(locationData);
        } else {
          await geoLocation.requestService;
          return Left(LocationFailure());
        }
      } else {
        await geoLocation.requestPermission;
        return Left(LocationFailure());
      }
    } else {
      return _getWeatherOfDayLocal();
    }
  }

  Future<Either<Failure, Weather>> _getWeatherOfDayRemote(LocationData locationData) async {
    try {
      Weather weather = await getWeatherRemoteDataSource.getWeatherOfDay(locationData.latitude, locationData.longitude);
      getWeatherLocalDataSource.storeWeatherOfDay(weather);
      return Right(weather);
    } on ServerException {
      return Left(ServerFailure());
    } on we.OpenWeatherAPIException {
      return Left(ServerFailure());
    }
  }

  Future<Either<Failure, Weather>> _getWeatherOfDayLocal() async {
    try {
      Weather weather = await getWeatherLocalDataSource.getWeatherOfDay();
      return Right(weather);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

}
