import 'package:myapp/features/data/models/WeatherModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'weather_data_local_data_source.dart';

class GetWeatherLocalDataSourceImpl extends GetWeatherLocalDataSource{
  SharedPreferences sharedPreferencesInst;
  GetWeatherLocalDataSourceImpl({this.sharedPreferencesInst});

  @override
  Future<List<WeatherModel>> getWeatherForecast() {
    // TODO: implement getWeatherForecast
    throw UnimplementedError();
  }

  @override
  Future<WeatherModel> getWeatherOfDay() {
    // TODO: implement getWeatherOfDay
    throw UnimplementedError();
  }

  @override
  void storeWeatherForecast(List<WeatherModel> weathers) {
    // TODO: implement storeWeatherForecast
  }

  @override
  void storeWeatherOfDay(WeatherModel weatherModel) {
    // TODO: implement storeWeatherOfDay
  }

}