import 'package:myapp/features/data/models/WeatherModel.dart';

abstract class GetWeatherLocalDataSource{
  Future<WeatherModel> getWeatherOfDay();
  Future<List<WeatherModel>> getWeatherForecast();
  void storeWeatherOfDay(WeatherModel weatherModel);
  void storeWeatherForecast(List<WeatherModel> weathers);
}