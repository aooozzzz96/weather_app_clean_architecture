import 'package:myapp/features/data/data_sources/weather_data_remote_data_source.dart';
import 'package:myapp/features/data/models/WeatherModel.dart';
import 'package:myapp/features/data/models/exceptions.dart';
import 'package:weather/weather.dart' as we;

class GetWeatherDataRemoteDataSourceImpl extends GetWeatherRemoteDataSource{
  final we.WeatherFactory weatherFactory;
  GetWeatherDataRemoteDataSourceImpl({
    this.weatherFactory
  });

  @override
  Future<List<WeatherModel>> getWeatherForecast(double lat, double long) async{
    // TODO: implement getWeatherForecast
    List<we.Weather> weathers = await weatherFactory.fiveDayForecastByLocation(lat, long);
    if(weathers == null) {
      throw ServerException();
    }
    return List.generate(weathers.length, (index) => WeatherModel(weather: weathers[index]));
  }

  @override
  Future<WeatherModel> getWeatherOfDay(double lat, double long) async{
    // TODO: implement getWeatherOfDay
     we.Weather weather = await weatherFactory.currentWeatherByLocation(lat, long);
     if(weather == null) {
       throw ServerException();
     }
     return WeatherModel(weather: weather);
  }

}