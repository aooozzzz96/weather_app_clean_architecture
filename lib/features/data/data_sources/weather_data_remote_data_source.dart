import 'package:myapp/features/data/models/WeatherModel.dart';

abstract class GetWeatherRemoteDataSource{
  Future<WeatherModel> getWeatherOfDay(double lat, double long);
  Future<List<WeatherModel>> getWeatherForecast(double lat, double long);
}