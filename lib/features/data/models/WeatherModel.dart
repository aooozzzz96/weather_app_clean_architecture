import 'package:myapp/features/domain/entities/Weather.dart';
import 'package:weather/weather.dart' as we;

class WeatherModel extends Weather {
  we.Weather weather;
  WeatherModel({this.weather}) : super(weather: weather);

  void fromJson(Map<String, dynamic> values) {
    weather = we.Weather(values);
  }

  Map<String, dynamic> toJson() {
    return weather.toJson();
  }
}