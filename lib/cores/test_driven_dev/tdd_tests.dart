import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:location/location.dart';
import 'package:mockito/mockito.dart';
import 'package:myapp/features/data/data_sources/weather_data_remote_data_source.dart';
import 'package:myapp/features/data/data_sources/weather_data_local_data_source.dart';
import 'package:myapp/features/data/data_sources/weather_data_remote_data_source_impl.dart';
import 'package:myapp/features/data/models/WeatherModel.dart';
import 'package:myapp/features/data/models/exceptions.dart';
import 'package:myapp/features/data/repositories/weather_repository_impl.dart';
import 'package:myapp/features/domain/entities/Failure.dart';
import 'package:myapp/features/domain/entities/Weather.dart';
import 'package:myapp/features/domain/repositories/weather_repository.dart';
import 'package:myapp/features/domain/usecases/get_weather_5_days.dart';
import 'package:myapp/features/domain/usecases/get_weather_this_day.dart';
import 'package:myapp/platform/get_location/geo_location.dart';
import 'package:myapp/platform/network_info/network_info.dart';
import 'package:weather/weather.dart' as we;

class MockRepo extends Mock implements WeatherRepository {}
class MockNetworkInfo extends Mock implements NetworkInfo{}
class MockRemoteDataSource extends Mock implements GetWeatherRemoteDataSource{}
class MockLocalDataSource extends Mock implements GetWeatherLocalDataSource{}
class MockGeoLocation extends Mock implements GeoLocation{}

class TDDTests{
  void testForRepoWithUseCase() {
    MockRepo mockRepo = MockRepo();
    GetWeatherThisDay _get;
    GetWeatherForecast _get_;
    final resultOfForecast = Right<Failure, List<Weather>>([Weather(weather: null)]);
    final resultOfWeather = Right<Failure, Weather>(Weather(weather: null));

    setUp(() {
      _get = GetWeatherThisDay(weatherRepository: mockRepo);
      _get_ = GetWeatherForecast(weatherRepository: mockRepo);
    });

    when(mockRepo.getWeatherThisDay()).thenAnswer((_) => Future.value(resultOfWeather));
    when(mockRepo.getWeatherForecast()).thenAnswer((_) => Future.value(resultOfForecast));

    test("test for use_case", () async{
      final value = await _get();
      final value2 = await _get_();
      verify(mockRepo.getWeatherThisDay());
      verify(mockRepo.getWeatherForecast());
      expect(value, resultOfWeather);
      expect(value2, resultOfForecast);
    });
  }
  void testForDataLayer() {
    WeatherRepositoryImpl _repository;
    MockNetworkInfo _networkInfo = MockNetworkInfo();
    MockRemoteDataSource _remoteDataSource = MockRemoteDataSource();
    MockLocalDataSource _localDataSource = MockLocalDataSource();
    MockGeoLocation _geoLocation = MockGeoLocation();
    we.Weather _weather;

    setUp(() {
      _weather = we.Weather(

          {
            "coord": {
              "lon": -122.08,
              "lat": 37.39
            },
            "weather": [
              {
                "id": 800,
                "main": "Clear",
                "description": "clear sky",
                "icon": "01d"
              }
            ],
            "base": "stations",
            "main": {
              "temp": 282.55,
              "feels_like": 281.86,
              "temp_min": 280.37,
              "temp_max": 284.26,
              "pressure": 1023,
              "humidity": 100
            },
            "visibility": 16093,
            "wind": {
              "speed": 1.5,
              "deg": 350
            },
            "clouds": {
              "all": 1
            },
            "dt": 1560350645,
            "sys": {
              "type": 1,
              "id": 5122,
              "message": 0.0139,
              "country": "US",
              "sunrise": 1560343627,
              "sunset": 1560396563
            },
            "timezone": -25200,
            "id": 420006353,
            "name": "Mountain View",
            "cod": 200
          }

      );
      _repository = WeatherRepositoryImpl(
        getWeatherLocalDataSource: _localDataSource,
        getWeatherRemoteDataSource: _remoteDataSource,
        networkInfo: _networkInfo,
        geoLocation: _geoLocation
      );
    });

    test("test for Data layer", () async{
      // when(_networkInfo.isConnected).thenAnswer((_) => Future.value(false));
      when(_networkInfo.isConnected).thenAnswer((_) => Future.value(true));
      when(_geoLocation.hasPermission).thenAnswer((_) => Future.value(PermissionStatus.grantedLimited));
      when(_geoLocation.serviceEnabled).thenAnswer((_) => Future.value(true));
      when(_geoLocation.location).thenAnswer((_) => Future.value(LocationData.fromMap({
        "latitude": 0.0,
        "longitude": 0.0
      })));
      // when(_remoteDataSource.getWeatherOfDay(0.0, 0.0)).thenThrow(ServerException());
      when(_remoteDataSource.getWeatherOfDay(0.0, 0.0)).thenAnswer((realInvocation) => Future.value(WeatherModel(weather: _weather)));
      when(_localDataSource.getWeatherOfDay()).thenThrow(CacheException());
      final value = await _repository.getWeatherThisDay();
      verify(_networkInfo.isConnected);
      verify(_remoteDataSource.getWeatherOfDay(0.0, 0.0));
      verify(_localDataSource.storeWeatherOfDay(any));
      // verify(_localDataSource.getWeatherOfDay());
      // expect(value, Left(CacheFailure()));
      expect(value, Right(WeatherModel(weather: _weather)));
    });
  }
  void testForDataSource() {}
}